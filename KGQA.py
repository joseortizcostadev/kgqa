

from sfsu_diffbot import *
from modules.QPM import *
from modules.MQFM import *
from modules.KGOEM import *
from modules.AESM import *


class KGQA(object):

    def __init__(self, question, question_id, verbose, kg_instance="dkg", api_key = None):
        self._question = question
        self._question_id = question_id
        self._verbose = verbose
        self._kg_instance = kg_instance
        self._api_key = api_key


    def set_api_key(self, api_key):
        self._api_key = api_key

    def answer(self):
        qpm = QPM(self._question)
        mqfm = MQFM(qpm)
        kgoem = KGOEM(mqfm, self._kg_instance, self._api_key)
        aesm = AESM(kgoem)
        answ =  aesm.answer_selection()
        if self._verbose:
            print("question_id: ", self._question_id)
            print("question: ", self._question)
            print("answer: ", answ)
        return answ





