## KGQA

KGQA is a question answering framework that takes different knowledge graghs instances as data source to provide 
a high quality answer to a given question. So far it supports Diffbot and Google knowledge graphs as intances.

## Code Example
This code example assumes that you have opened the project in a python IDE (e.g Pycharm) and create a new file to run 
the following code.
    
    from KGQA import *
    from instances import *
    question = "How tall is Mount McKinley?"
    question_id = 1
    diffbot_instance = Intances.Diffbot()
    google_instance = Instances.Google()
    dkg_key = "xxxxxxxxx" # put your diffbot Knowledge Graph API Key
    gkg_key = "xxxxxxxxx" # put your google knowledge graph API Key
    dkgqa = KGQA(question, question_id, verbose=True, kg_instance=diffbot_instance, api_key=key) # using DKG
    gkgqa = KGQA(question, question_id, verbose=True, kg_instance=google_instance, api_key=key) # using GKG
    dkgqa_answer = dkgqa.answer()
    gkgqa_answer = gkgqa.answer()

## Motivation

This project was part of the requeriments for my MS of Science in Computer Science, SFSU, Summer 2018

## Installation

1. Install pip (python 2.x) or pip3 (python 3.x)
2. Clone or download kgqa project
3. Install project requeriments (file is in ~/kgqa)

       pip install -r requirements.txt
       
       
4. Open the project in your prefered IDE
5. Create a file and run the example code
6. For running this project from terminal, you'll need to create a __Main__ file to point out the
   reference starting point file.

## API Reference

KGQA uses custom APIs included in the project in order to extract data from the knowledge graph instances supported

## Tests

The test.py file contains methods to perform evaluation experiments using the system. Datasets are not included in this 
repository since they contain thousends of entries. If you need access to the datasets, email me at jortizco@mail.sfsu.edu

## Contributors

Jose Ortiz Costa, MS in Computer Science. jortizco@mail.sfsu.edu

Professor Anagha Kulkarni, from San Francisco State University. ak@sfsu.edu

## License

This repository is private. Only administrators and contributors are allowed to edit or contribute to the project.