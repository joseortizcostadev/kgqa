"""
This file implements the Answer Extraction Selection Module (AESM)

Package: kgqa

Author: Jose Ortiz

"""
# -*- coding: utf-8 -*-
from __future__ import division
import re
import math
import nltk, string
import numpy
from sklearn.feature_extraction.text import TfidfVectorizer
import statistics
import spacy

class AESM(object):
    """
    AESM takes as imput the kgoem object from the KGOEM class which provide high quality objects to be analyzed. Then, 
    this module returns the best answer found (if any)
    """
    def __init__(self, kgoem, apply_kgqa_score=True):
        """
        Constructor
        :param kgoem: 
        :param apply_kgqa_score: 
        """
        self._oem = kgoem
        self._instance = kgoem.instance()
        data = kgoem.encapsulate_objects_mq(True)
        self._best_query = data[0]
        self._top_objects = self.kgqa_score(data[1], apply_kgqa_score)
        self._sentences = self._extract_sentences(self._top_objects)
        self._stemmer = nltk.stem.porter.PorterStemmer()
        self._remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
        self._ranked_sentences_data = self._ranked_sentences()
        self._npl = spacy.load('en')

    def objects_to_string(self, objects, field='url'):
        for object in objects:
            if hasattr(object, field):
                print(object.url())

    def kgqa_score(self, objects, apply_kgqa_score):
        scores = {}
        new_objects = []
        if apply_kgqa_score:
            index = 1
            for object in objects:
                if self._instance == "dkg":
                    confidence = (1 - (index / len(objects))) * 0.4
                    tags_sorted_by_score = object.tags_sorted_by_score()
                    tags_sorted_by_count = object.tags_sorted_by_count()
                    if len(tags_sorted_by_score) > 0:
                        tag_score = tags_sorted_by_score[0].score() * 0.3
                    else:
                        tag_score = 0
                    if len(tags_sorted_by_count) > 0:
                        tag_count = tags_sorted_by_count[0].count() * 0.3
                    else:
                        tag_count = 0
                    score = confidence + tag_score + tag_count
                    scores[score] = object
                    index += 1
                elif self._instance == "gkg":
                    confidence = 0
                    object_data, tag_score, tag_count = object[0], object[1], object[2]
                    score = confidence + tag_score + tag_count
                    scores[score] = object
                    score = confidence + tag_score + tag_count
                    scores[score] = object_data
                    index += 1
            new_scores = sorted(scores, reverse=True)
            for s in new_scores:
                new_objects.append(scores[s])
            return new_objects

        return objects

    def stem_tokens(self, tokens):
        return [self._stemmer.stem(item) for item in tokens]

    def normalize(self, text):
        return self.stem_tokens(nltk.word_tokenize(text.lower().translate(self._remove_punctuation_map)))

    def vectorizer(self):
        return TfidfVectorizer(tokenizer=self.normalize, stop_words='english')

    def cosine_sim(self, text1, text2):
        tfidf = self.vectorizer().fit_transform([text1, text2])
        return ((tfidf * tfidf.T).A)[0, 1]

    def split_into_sentences(self, text):
        return list(set(self.split_into_sentences_add(text.strip())))  # list(set(nltk.sent_tokenize(text.strip())))

    def remove_non_alphanumeric(self, sentence):
        pattern = re.compile('\W')
        return re.sub(pattern, ' ', sentence)

    def remove_duplicates(self, queries):
        seen = set()
        seen_add = seen.add
        return [x for x in queries if not (x in seen or seen_add(x))]

    def _extract_sentences(self, encapsulated_objects):
        sentences = []
        for object in encapsulated_objects:
            try:
                text = ""
                if self._instance == "dkg":
                    if hasattr(object, 'text') and object is not None:
                        text = object.text().lower()
                    else:
                        text = object
                elif self._instance == "gkg":
                    for tag in object:
                        text += "\n" + tag["description"].lower()
                sents = self.split_into_sentences(text)
                for sent in sents:
                     if self.is_valid_sentence(sent):
                        sentences.append(sent.strip())
            except:
                continue
        return self.remove_duplicates(sentences)

    def is_valid_sentence(self, sentence):
        if "?" in sentence:
            return False
        elif len(sentence.split()) <= 3:
            return False
        return True

    def sp_sim(self, q1, q2):
        q1s = self._npl(q1)
        q2s = self._npl(q2)
        return q1s.similarity(q2s)

    def _ranked_sentences(self):
        scored_sents = []
        query = self._oem.query()
        for sentence in self._sentences:
            score = self.cosine_sim(query, sentence)  # + self.inverse_delta_distance(sentence)
            if score > 0:
                scored_sents.append((score, sentence))
        sorted_sentences = sorted(scored_sents, reverse=True)
        return sorted_sentences

    def _sentence_data(self, rank):
        return self._ranked_sentences()[rank]

    def sentence_at(self, rank):
        index = self._sentence_data(rank)[2]
        return self._sentences[index]

    def sentence_score_at(self, rank):
        return self._ranked_sentences()[rank][0]

    def all_scores(self):
        scores = []
        if len(self._ranked_sentences()) > 0:
            min = self._ranked_sentences()[-1][0]
            max = self._ranked_sentences()[0][0]
            for data in self._ranked_sentences():
                try:
                    score = self.normalize_score(min, max, data[0])
                except:
                    score = 0.10
                if score > 0:
                    scores.append(score)
        return scores

    def scores_std(self, scores):
        return numpy.std(scores)

    def scores_mean(self, scores):
        return numpy.mean(scores)

    def scores_median(self, scores):
        return numpy.median(scores)

    def inverse_delta_distance(self, sentence):
        positions = []
        tok_positions = 1
        sen_tokens = sentence.split()
        q_tokens = self._oem.query_tokens()
        len_sen_tokens = len(sen_tokens) - 1
        for token in sen_tokens:
            if token in q_tokens:
                positions.append(tok_positions)
            tok_positions += 1
        print(positions)
        return len_sen_tokens / self.delta_distance(positions)

    def delta_distance(self, positions):
        distance = 0
        len_pos = len(positions)
        if len_pos == 1:
            return 0
        else:
            for i in range(len(positions) - 1):
                distance += positions[i + 1] - positions[i]
        return distance

    def answer_selection(self):
        selected_sentences = []
        scores = []
        sentences = []
        ranked_sentences = self._ranked_sentences_data
        for data in ranked_sentences:
            scores.append(data[0])
            sentences.append(data[1])
        mean = numpy.mean(scores)
        sentence_index = 0
        for score in scores:
            if score >= mean:
                selected_sent = sentences[sentence_index]
                selected_sentences.append(selected_sent)
                sentence_index += 1
                if sentence_index > 5:
                    break
        best_ans = " ".join(selected_sentences)
        best_ans = best_ans.replace("dbo:abstract", "")
        return best_ans

    def normalize_score(self, min, max, score):
        return (score - min) / (max - min)

    def split_into_sentences_add(self, text):
        caps = "([A-Z])"
        prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
        suffixes = "(Inc|Ltd|Jr|Sr|Co)"
        starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
        acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
        websites = "[.](com|net|org|io|gov)"
        text = " " + text + "  "
        text = text.replace("\n", " ")
        text = re.sub(prefixes, "\\1<prd>", text)
        text = re.sub(websites, "<prd>\\1", text)
        if "Ph.D" in text: text = text.replace("Ph.D.", "Ph<prd>D<prd>")
        text = re.sub("\s" + caps + "[.] ", " \\1<prd> ", text)
        text = re.sub(acronyms + " " + starters, "\\1<stop> \\2", text)
        text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>\\3<prd>", text)
        text = re.sub(caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>", text)
        text = re.sub(" " + suffixes + "[.] " + starters, " \\1<stop> \\2", text)
        text = re.sub(" " + suffixes + "[.]", " \\1<prd>", text)
        text = re.sub(" " + caps + "[.]", " \\1<prd>", text)
        if "”" in text: text = text.replace(".”", "”.")
        if "\"" in text: text = text.replace(".\"", "\".")
        if "!" in text: text = text.replace("!\"", "\"!")
        if "?" in text: text = text.replace("?\"", "\"?")
        text = text.replace(".", ".<stop>")
        text = text.replace("?", "?<stop>")
        text = text.replace("!", "!<stop>")
        text = text.replace("<prd>", ".")
        sentences = text.split("<stop>")
        sentences = sentences[:-1]
        sentences = [s.strip() for s in sentences]
        return sentences

    def mean_num_objects(self):
        objects = self._oem.number_objects_list()
        if len(objects) > 0:
            return statistics.mean(objects)
        return 0.00

    def median_num_objects(self):
        objects = self._oem.number_objects_list()
        if len(objects) > 0:
            sorted_num_objects = sorted(objects)
            return statistics.median(sorted_num_objects)
        return 0.00

    def best_answer_baseline(self, max_sentences=4):
        best_answer = ""
        scores = []
        sentences = self.split_into_sentences(self._oem.encapsulate_objects_baseline())
        for sentence in sentences:
            score = self.tf(sentence.lower()) / len(self._oem.query().lower())
            if score > 0:
                scores.append((score, sentence))
        scores_sorted = sorted(scores, reverse=True)
        for i in range(0, len(scores_sorted)):
            if i == max_sentences:
                break
            best_answer += scores_sorted[i][1]
        return best_answer

    def bas_sentence_score(self, sentence):
        return self.inverse_delta_distance(sentence)

    def best_query_found(self):
        return self._best_query

    def tf(self, sentence):
        tf = 0
        for token in self._oem.query_tokens():
            if sentence.count(token) > 0:
                tf += 1
        return tf
