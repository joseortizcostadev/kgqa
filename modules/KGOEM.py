"""
This file implements the Knowledge Graph Object Extraction Module (KGOEM)

Package: kgqa

Author: Jose Ortiz

"""
import math
import re
import spacy
from google_kg_client.GKGAPI import GKGAPI
from sfsu_diffbot.sfsu_diffbot_client import *


class KGOEM(object):
    """
    KGOEM will take as input the set of multiple queries and returns high quality objects from the knowledge graph 
    """
    def __init__(self, mqfm, kg_instance="dkg", api_key = None):
        """
        OEM Constructor
        :param mqfm: MQFM module
        """
        self._mqfm = mqfm
        self._server = kg_instance
        self._api_key = api_key
        self._client = self.get_client()
        self._response = None
        self._query = self._mqfm.query()
        self._tokens = mqfm.tokens()
        self._uris = []
        self._hits = 0
        self._best_query = self._query
        self._objects_added = []
        self._multiqueries = self._mqfm.multiquery()  # uncomment this for multiquery. Otherwise, baseline is executed
        self._best_basiline_object = None
        self._num_objects = []



    def get_client(self):
        """
        
        :return: The KG instance client
        """
        client = None
        if self._server == "dkg":
            client  = Diffbot(self._api_key).client()
        elif self._server == "gkg":
            client = GKGAPI(self._api_key)
            client.set_queries(["world wide web"])
        return client

    def search(self, query):
        """

        :param query: 
        :return: 
        """
        return self._client.search(query)

    def article(self, url):
        """

        :param url: 
        :return: 
        """
        return self._client.article(url)

    def product(self, url):
        """

        :param url: 
        :return: 
        """
        return self._client.product(url)

    def objects(self, response):
        """

        :param response: 
        :return: 
        """
        return response.objects()

    def baseline(self, include_tags=False):
        """
        BKGQA baseline implementation
        :param include_tags: 
        :return: 
        """
        best_answer = ""
        nlp = spacy.load('en')
        max_scores = []
        query_nlp = nlp(self._query)
        main_response = self._client.search(self._mqfm.query())
        if main_response.hits() is not None and main_response.hits() > 0:
            self._best_query = main_response.query_info().fullQuery()
        objects = main_response.objects()
        text = ""
        if len(objects) > 0:
            object = objects[0]
            if object.type() == 'article':
                if hasattr(object, 'text'):
                    text = object.text()
        sentences = self.split_into_sentences_add(text)
        for sentence in sentences:
            sent_nlp = nlp(sentence)
            score = query_nlp.similarity(sent_nlp)
            max_scores.append((score, sentence))
        sorted_scores = sorted(max_scores, reverse=True)
        sent_index = 0
        for sent in sorted_scores:
            if sent_index < 3:
                best_answer += ". " + sent[1]
                sent_index += 1
        return self._query, best_answer

    def split_into_sentences_add(self, text):
        """
        Splits text into sentences
        :param text: 
        :return: a list of sentences from the text
        """
        caps = "([A-Z])"
        prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
        suffixes = "(Inc|Ltd|Jr|Sr|Co)"
        starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
        acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
        websites = "[.](com|net|org|io|gov)"
        text = " " + text + "  "
        text = text.replace("\n", " ")
        text = re.sub(prefixes, "\\1<prd>", text)
        text = re.sub(websites, "<prd>\\1", text)
        if "Ph.D" in text: text = text.replace("Ph.D.", "Ph<prd>D<prd>")
        text = re.sub("\s" + caps + "[.] ", " \\1<prd> ", text)
        text = re.sub(acronyms + " " + starters, "\\1<stop> \\2", text)
        text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>\\3<prd>", text)
        text = re.sub(caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>", text)
        text = re.sub(" " + suffixes + "[.] " + starters, " \\1<stop> \\2", text)
        text = re.sub(" " + suffixes + "[.]", " \\1<prd>", text)
        text = re.sub(" " + caps + "[.]", " \\1<prd>", text)
        if "”" in text: text = text.replace(".”", "”.")
        if "\"" in text: text = text.replace(".\"", "\".")
        if "!" in text: text = text.replace("!\"", "\"!")
        if "?" in text: text = text.replace("?\"", "\"?")
        text = text.replace(".", ".<stop>")
        text = text.replace("?", "?<stop>")
        text = text.replace("!", "!<stop>")
        text = text.replace("<prd>", ".")
        sentences = text.split("<stop>")
        sentences = sentences[:-1]
        sentences = [s.strip() for s in sentences]
        return sentences

    def encapsulate_objects_mq(self, with_tags=True):
        """
        Encapulates objects 
        :param with_tags: 
        :return: 
        """
        encapsulated_objects = []
        multiqueries = self._multiqueries
        if self._server == "dkg":
            for query in multiqueries:
                obj_data = self._encapsulate_objects_mq_helper(query, with_tags)
                for object in obj_data:
                    encapsulated_objects.append(object)
        elif self._server == "gkg":
           encapsulated_objects = self.gkg_objects(self.multiquery())
        return self._best_query, encapsulated_objects

    def twox(self, score, next_object_score):
        """
        2x computation
        :param score: 
        :param next_object_score: 
        :return: 
        """
        if next_object_score < score:
            return False
        return True

    def gkg_objects(self, queries):
        """
        
        :param queries: 
        :return: google knowledge graphs objects
        """
        self._client.set_queries(queries)
        objects = self._client.objects()
        return objects

    def _encapsulate_objects_mq_helper(self, query, include_tags):
        """
        Encapsulates objects from a single query
        :param query: 
        :param include_tags: 
        :return: A list of encapsulated objects 
        """
        objects_list = []
        main_response = self._client.search(query)
        hits = main_response.hits()
        objects = main_response.objects()
        last_score = 0
        num_objects = 0
        if len(objects) > 0:
            if hits > self._hits and query != self._query:
                self._best_query = query
                self._hits = hits
            for object in objects:
                if hasattr(object, 'text') and hasattr(object,
                                                       'url') and object.url() not in self._objects_added and object.humanLanguage() == 'en':
                    self._objects_added.append(object.url())
                    conf = self.confidence(object)
                    if conf > last_score:
                        last_score = conf
                        num_objects += 1
                    else:
                        break
                    objects_list.append(object)
                    if include_tags:
                        tags = object.tags_sorted_by_score()
                        if len(tags) > 0:
                            for tag in tags:
                                print(tag.label())
                                print(tag.score())
                                for token in self.query().split():
                                    uri = tag.uri()
                                    if token in tag.uri() and uri not in self._uris:
                                        response = self._client.article(uri)
                                        objs = response.objects()
                                        if len(objs) > 0:
                                            tag_obj = objs[0]
                                            if hasattr(tag_obj, 'text'):
                                                self._uris.append(uri)
                                                objects_list.append(tag_obj)
            self._num_objects.append(num_objects)
        return objects_list

    def confidence(self, object):
        """
        Computes the confidence score
        :param object: 
        :return: the confidence score
        """
        confidence_count = 0
        if hasattr(object, "text"):
            text = object.text()
            grams = self._mqfm.grams()
            for gram in grams:
                if gram in text:
                    confidence_count += 1
            return confidence_count / len(grams)
        else:
            return 0.00

    def number_objects_list(self):

        return self._num_objects

    def query(self):
        return self._query

    def query_tokens(self):
        return self._tokens

    def multiquery(self):
        return self._mqfm.multiquery()

    def best_query(self):
        return self._best_query

    def tf(sekf, word, blob):
        """
        
        :param word: 
        :param blob: a blob object 
        :return: the term frequency of the word 
        """
        return blob.count(word) / len(blob.split())

    def n_containing(self, word, bloblist):
        return sum(1 for blob in bloblist if word in blob.split())

    def idf(self, word, bloblist):
        return math.log(len(bloblist) / (1 + self.n_containing(word, bloblist)))

    def tfidf(self, word, blob, bloblist):
        return self.tf(word, blob) * self.idf(word, bloblist)

    def top_doc(self, all_docs=False):
        """
        
        :param all_docs: 
        :return: the top high quality object
        """
        query = self._query
        texts = []
        weight = 0.00
        objects = self.encapsulate_objects_mq()[1]
        top_object = objects[0]
        if all_docs:
            return self._best_query, objects
        for object in objects:
            try:
                if hasattr(object, 'text'):
                    texts.append(object.text())
            except:
                continue
        index = 0
        tokens = query.split()
        for text in texts:
            if len(text) > 2:
                w = 0.00
                for token in tokens:
                    token = token.replace("\"", "")
                    w += self.tfidf(token, text, texts)
                if w > weight:
                    weight = w
                    top_object = objects[index]

            index += 1
        return self._best_query, [top_object]

    def instance(self):
        return self._server

