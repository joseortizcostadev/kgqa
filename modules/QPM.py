"""
This file implements the Question Preprocessing Module (QPM)

Package: kgqa

Author: Jose Ortiz

"""


import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

class QPM(object):
    """
        QPM preprocess the original question implementing a basic gramatical correction, and sanitizing the question to 
        remove junk data.
    """

    def __init__(self, question):
        """
        Class constructor.
        :param free_text: 
        """

        self._free_text = self.first_q(question)
        self._query = self.get_sanitazed_sentence()

    def first_q(self, text):
        """
        :param text: the original text
        :return: the text without the ? character if any
        """
        return text.split("?")[0]

    def remove_non_alphanumeric(self, query):
        """
        :param query: the query
        :return: the query containing only alphanumeric characters
        """
        pattern = re.compile('\W')
        return re.sub(pattern, ' ', query)

    def get_sanitazed_sentence(self):
        """
        :return: the sanitazed question
        """
        stop_words = set(stopwords.words('english'))
        stop_words.update(('Where', 'Who', 'Whose', 'What', 'Why', 'How', 'and', 'I', 'A', 'And', 'So', 'arnt', 'This',
                           'When', 'It', 'many', 'Many', 'so', 'cant',
                           'Yes', 'yes', 'No', 'no', 'These', 'these', 'is', 'are', 'Do', "Are", "About", "For", "Is"))
        stop_words.remove('own')
        stop_words.remove('too')
        word_tokens = word_tokenize(self._free_text)
        filtered_sentence = [w for w in word_tokens if not w in stop_words]
        sentence = self.remove_non_alphanumeric(" ".join(filtered_sentence))
        sentence = sentence.replace(" s ", "")
        sentence = sentence.replace("\"", "")
        return sentence

    def query(self):
        """
        :return: the original question before being preprocessed.
        """
        return self._query
