from KGQA import *
from instances import *
question = "How tall is Mount McKinley?"
question_id = 1
diffbot_instance = Instances.Diffbot()
google_instance = Instances.Google()
dkg_key = "xxxxxxxxx" # put your diffbot Knowledge Graph API Key
gkg_key = "xxxxxxxxx" # put your google knowledge graph API Key
dkgqa = KGQA(question, question_id, verbose=True, kg_instance=diffbot_instance, api_key=key) # using DKG
gkgqa = KGQA(question, question_id, verbose=True, kg_instance=google_instance, api_key=key) # using GKG
dkgqa_answer = dkgqa.answer()
gkgqa_answer = gkgqa.answer()